# Example-Graphics

Vulkan Rendering Examples

## Building

The project depends on
```
cmake
vulkan
glfw
glm
```
Most of this project's examples are from the official
[vulkan-tutorial](vulkan-tutorial.com).

Execute
```bash
env/setup.sh
env/build.sh
```
To build the entire project.
