/**
 * This example is from
 * <a href="https://vulkan-tutorial.com/Development_environment">Vulkan
 * Tutorial</a>
 *
 * In this example we create a window and print some diagnostic information
 * regarding Vulkan.
 *
 *
 */
#include <iostream>

#include "version.hpp"
#include "headers.hpp"

int main()
{
	glfwInit();

	// GLFW_NO_API inhibits the OpenGL contetx so we can use Vulkan instead
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow* const window
	  = glfwCreateWindow(800, 600, TARGET_NAME, nullptr, nullptr);

	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::cout << extensionCount << " extensions supported\n";

	// Ensure that we can compile
	glm::mat4 matrix;
	glm::vec4 vec;
	auto      test = matrix * vec;

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();

	return 0;
}
