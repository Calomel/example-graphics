/**

* This example is from
 * <a href="https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Base_code">
 * Vulkan Tutorial</a>
 *
 * In this example we draw a triangle. Note that this is tremendously more
 * complicated than OpenGL.
 * regarding Vulkan.
 */
#include <iostream>
#include <vector>
#include <cstring>

#include "version.hpp"
#include "headers.hpp"

/*
 * This variable's scope must encompass the instance. Otherwise we get
 * SIGSEGV. Curiously setting it as an instance variable does not seem to work.
 */
#ifndef NDEBUG
std::vector<char const*> const VALIDATION_LAYERS = {
	"VK_LAYER_KHRONOS_validation",
};
#endif

class Application
{
public:
	Application()
	{
		initWindow();
		initVulkan();
	}
	~Application()
	{
		vkDestroyInstance(instance_, nullptr);
		glfwDestroyWindow(window_);
		glfwTerminate();
	}

	/**
	 * This is the application's main loop
	 */
	void exec()
	{
		while (!glfwWindowShouldClose(window_))
		{
			glfwPollEvents();
		}
	}

private:
	void initWindow()
	{
		constexpr uint32_t WINDOW_WIDTH = 800, WINDOW_HEIGHT = 600;
		// Inhibit GLFW from creating OpenGL context
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		// Turn off resizing. This will be handled later since it adds more
		// complexity.
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		// Now create the window object.
		window_ = glfwCreateWindow(
		  WINDOW_WIDTH, WINDOW_HEIGHT, TARGET_NAME, nullptr, nullptr);
	}

	void initVulkan()
	{
		// The first step is to create the Vulkan instance.

		// Many structs in Vulkan requiers the sType to be set explicitly.
		VkApplicationInfo appInfo;
		appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName   = TARGET_NAME;
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName        = TARGET_NAME " Engine";
		appInfo.engineVersion      = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion         = VK_API_VERSION_1_0;

		VkInstanceCreateInfo createInfo;
		createInfo.sType            = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		// Specify the extensions for integration with the window system.
		uint32_t           glfwExtensionsCount = 0;
		char const** const glfwExtensions
		  = glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);
		createInfo.enabledExtensionCount   = glfwExtensionsCount;
		createInfo.ppEnabledExtensionNames = glfwExtensions;

		// Peek inside the glfw extensions
		std::cout << "GLFW Extensions:\n";
		for (int i = 0; i < glfwExtensionsCount; ++i)
			std::cout << '\t' << glfwExtensions[i] << '\n';
#ifndef NDEBUG
		/**
		 * Vulkan debugging is done through
		 * <a
		 * href="https://vulkan-tutorial.com/en/Drawing_a_triangle/Setup/Validation_layers">
		 * validation layers</a>.
		 */
		if (!checkValidationLayerSupport(VALIDATION_LAYERS))
		{
			throw std::runtime_error("Validation layer check failed");
		}

		createInfo.enabledLayerCount   = (uint32_t) VALIDATION_LAYERS.size();
		createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
#else
		createInfo.enabledLayerCount = 0;
#endif


		// With the creation info established we can create the Vulkan instance.
		if (vkCreateInstance(&createInfo, nullptr, &instance_) != VK_SUCCESS)
			throw std::runtime_error("Failed to create Vulkan instance");

		// Check for extension support
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> extensions(extensionCount);
		vkEnumerateInstanceExtensionProperties(
		  nullptr, &extensionCount, extensions.data());
		std::cout << "Vulkan Extensions:\n";
		for (const auto& extension : extensions)
			std::cout << '\t' << extension.extensionName << '\n';
	}

	bool checkValidationLayerSupport(std::vector<char const*> const layers)
	{
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		assert(layers.size() == 1);

		bool hit = false;
		std::cout << "Vulkan Validation Layers\n";
		for (auto const& p : availableLayers)
		{
			std::cout << '\t' << p.layerName << '\n';
			if (std::strcmp(p.layerName, layers[0]) == 0)
				hit = true;
		}
		return hit;
	}

	GLFWwindow* window_;
	VkInstance  instance_;
};

int main()
{
	try
	{
		glfwInit();
		Application app;
		app.exec();
	}
	catch (std::exception const& e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
