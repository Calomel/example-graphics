#ifndef EG_VERSION_HPP
#define EG_VERSION_HPP

// clang-format off
#ifndef TARGET_NAME
#	define TARGET_NAME "Example-Graphics"
#endif
// clang-format on

#endif  // !EG_VERSION_HPP
